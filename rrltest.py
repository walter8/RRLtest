#!/usr/bin/python3

import dns.message
import dns.query
import socket
import argparse
import time
import select

class RRLtest():
    def __init__(self):
        self.number_of_queries = 100
        self.query_name = None
        self.record_type = 'A'
        self.short = True
        self.host = None 
        self.single = False
        self.port = 53
        self.size = 1500
        self.wait_time = 1
        self.id_list = []
        self.answer_list = [None] * self.number_of_queries


    def setparams(self,target,name,number,query_type,short=True,port=53):
        self.host = target 
        self.query_name = name
        self.number_of_queries = number
        self.record_type = query_type
        self.short = short
        self.port = port
        self.id_list = []
        self.answer_list = [None] * self.number_of_queries  

    def getargs(self):

        parser = argparse.ArgumentParser(description='DNS rrl tester')
        parser.add_argument('target', action='store') 
        parser.add_argument('query_name', action="store")
        parser.add_argument('-n', action='store', dest='number_of_queries',type=int, default=100, help='Number of times to send query, default is 100')
        parser.add_argument('-s', action='store_true', dest='single', default=False, help='Use a single socket for queries')
        parser.add_argument('-t', action='store', dest='query_type', default='A', help='Query type, default is A')
        parser.add_argument('-l', action='store_false', dest='short', default=True, help='Long printing format')

        args = parser.parse_args()

        self.number_of_queries = args.number_of_queries
        self.query_name = args.query_name
        self.record_type = args.query_type
        self.short = args.short
        self.host = args.target 
        self.single = args.single
        #self.port = 53
        #self.size = 1500
        self.id_list = []
        self.answer_list = [None] * self.number_of_queries


    def run_multi(self):
        inputs = []
        for i in range(self.number_of_queries):
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect((self.host,self.port))
            q = dns.message.make_query(self.query_name, self.record_type)
            ident = q.id
            while ident in self.id_list: #if we are already have that id, make another
                q = dns.message.make_query(self.query_name, self.record_type)
                ident = q.id
            self.id_list.append(ident)
            s.send(q.to_wire())
            inputs.append(s)

        time.sleep(self.wait_time) #wait for all responses to come in

        readable, writable, exceptional = select.select(inputs, [], []) 
        for sock in readable:
            data = sock.recv(self.size)
            mess = dns.message.from_wire(data)
            index = self.id_list.index(mess.id)
            self.answer_list[index] = mess


    def run_single(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(0.0000001)
        s.connect((self.host,self.port))
        for i in range(self.number_of_queries):
            q = dns.message.make_query(self.query_name, self.record_type)
            ident = q.id
            while ident in self.id_list: #if we are already have that id, make another
                q = dns.message.make_query(self.query_name, self.record_type)
                ident = q.id
            self.id_list.append(ident)
            s.send(q.to_wire())
            data = 'NOT NONE' 
            if i%10 == 9:
                while data:
                    try:
                        data = s.recv(1500)
                        mess = dns.message.from_wire(data)
                        index = self.id_list.index(mess.id)
                        self.answer_list[index] = mess
                    except socket.timeout:
                        break

        #wait and get remaining answers
        s.settimeout(self.wait_time)

        while data:
            try:
                data = s.recv(1500)
                mess = dns.message.from_wire(data)
                index = self.id_list.index(mess.id)
                self.answer_list[index] = mess
            except socket.timeout:
                break

    def print_results(self):
        no_response = 0
        truncated_answer = 0
        normal_answer = 0
        empty_answer = 0
        refused = 0
        for j in range(self.number_of_queries):
            if self.answer_list[j] == None:
                no_response += 1
                if self.short:
                    print('.',end='')
                else:
                    print('Query', j+1, 'with id', hex(self.id_list[j]), 'recieved no response')
            elif self.answer_list[j].flags & 512 == 512: #bitwise, true if truncated flag is set
                truncated_answer += 1
                if self.short:
                    print('T',end='')
                else:
                   print('Query', j+1, 'with id', hex(self.id_list[j]), 'recieved Truncated bit')
            elif self.answer_list[j].answer == []:
                empty_answer += 1
                if self.short:
                    print('E',end='')
                else:
                   print('Query', j+1, 'with id', hex(self.id_list[j]), 'recieved empty answer section')
            elif self.answer_list[j].rcode() == 5: #if query is refused
                refused += 1
                if self.short:
                    print('R',end='')
                else:
                   print('Query', j+1, 'with id', hex(self.id_list[j]), 'was refused')
            else:
                normal_answer += 1
                if self.short:
                    print('A',end='')
                else:
                   print('Query', j+1, 'with id', hex(self.id_list[j]), 'recieved answer', self.answer_list[j].answer[0])

        print('\nResults:')
        print(self.number_of_queries, 'total queries')
        print(normal_answer, 'normal responses')
        print(truncated_answer, 'truncated responses')
        print(no_response, 'no responses')
        if refused > 0:
            print(refused,'refused queries')
        if empty_answer > 0:
            print(empty_answer, 'empty answers')

if __name__ == "__main__":
    a = RRLtest()
    a.getargs()
    if not a.single:
        a.run_multi()
    else:
        a.run_single()
    a.print_results()
